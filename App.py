from flask import Flask,jsonify,request
from Product import products

app = Flask(__name__)


@app.route("/ping")
def ping():
    return jsonify({"message":"pong!!"})

@app.route("/products")
def get_products():
    return jsonify({"products": products , "message":"LISTA DE PRODUCTOS"})

@app.route("/products/<string:product_name>")
def get_product(product_name):
    product_encontrado = ""
    for product in products:
        if (product["name"] == product_name):
            product_encontrado = product
    if product_encontrado != "":
        return jsonify({"producto":product_encontrado})
    else:
        return({"message":"producto no econtrado"})

@app.route("/products", methods=["POST"])
def add_product():
    new_product={
        "name": request.json["name"],
        "price": request.json["price"],
        "quantity": request.json["quantity"]
    }
    products.append(new_product)
    return jsonify({"message":"producto agregado" ,"product": new_product})

@app.route("/products/<string:product_name>",methods=["PUT"])
def edit_product(product_name):
    product_found = [product for product in products if product["name"]==product_name]
    if(len(product_found)>0):
        product_found[0]["name"] = request.json["name"]
        product_found[0]["price"] = request.json["price"]
        product_found[0]["quantity"] = request.json["quantity"]
        return jsonify({
            "message":"producto modificado",
            "product":product_found[0]
            })
    return jsonify({"message":"producto no encontrado"})


@app.route("/products/<string:product_name>",methods=["DELETE"])
def delete_product(product_name):
    product_found = [product for product in products if product["name"]==product_name]
    if(len(product_found)>0):
        products.remove(product_found[0])
        return jsonify({
            "message":"producto removido",
            "product":product_found
        })
    return jsonify({"message":"producto no encontrado"})

if __name__== "__main__":
    app.run(debug=True, port=4000)
